const appRoot = document.getElementById('app-root');

//title
const title = document.createElement('h1');
title.className = 'country-search-title title';
title.textContent = 'Countries Search';
appRoot.append(title);

const mainBlock = document.createElement('div');
mainBlock.className = 'mainBlock';
appRoot.append(mainBlock);

/////////////////radio////////

//main RadioBlock
const mainRadioBlock = document.createElement('div');
mainRadioBlock.className = 'mainRadioBlock';
mainBlock.append(mainRadioBlock);

//title Radio
const radioTitle = document.createElement('h3');
radioTitle.className = 'radio-title title';
radioTitle.textContent = 'Please choose type of search:';
mainRadioBlock.append(radioTitle);

//block1
const radioBlock1 = document.createElement('div');
mainBlock.append(radioBlock1);

//radio1
const radioButton1 = document.createElement('input');
radioButton1.id = 'id1';
radioButton1.setAttribute('type', 'radio');
radioButton1.setAttribute('name', 'radio');
radioBlock1.append(radioButton1);

//label1
const radioLabel1 = document.createElement('label');
radioLabel1.className = 'radio-label';
radioLabel1.textContent = 'By Region';
radioBlock1.append(radioLabel1);

//block2
const radioBlock2 = document.createElement('div');
mainBlock.append(radioBlock2);

//radio2
const radioButton2 = document.createElement('input');
radioButton2.id = 'id2';
radioButton2.setAttribute('type', 'radio');
radioButton2.setAttribute('name', 'radio');
radioBlock2.append(radioButton2);

//label2
const radioLabel2 = document.createElement('label');
radioLabel2.className = 'radio-label';
radioLabel2.textContent = 'By Language';
radioBlock2.append(radioLabel2);

//add in a main block
mainRadioBlock.append(radioBlock1);
mainRadioBlock.append(radioBlock2);

const arrayReg = externalService.getRegionsList();
const arrayLang = externalService.getLanguagesList();
/////////////////////////////////////select

const dropBlock = document.createElement('div');
mainBlock.append(dropBlock);

//change select value with radio button

let radio_buttons = document.getElementsByName('radio');

for (let i = 0; i < radio_buttons.length; i++) {
    radio_buttons[i].addEventListener('change', setDropDown);
}
function setDropDown() {}

// функция для получения данных по регионам

function listOfRegions() {
    let arr1 = [];
    arr1 = externalService.getRegionsList();
    let h3 = document.createElement('h3');
    h3.className = 'title';
    h3.innerHTML = 'Please choose search query';
    let select = document.createElement('select');
    select.id = 'mySelect';
    let option1 = document.createElement('option');
    option1.text = 'Select value';
    select.append(option1);

    for (let i = 0; i < arr1.length; i++) {
        let option = document.createElement('option');
        option.text = arr1[i];
        select.append(option);
    }
    let defaultText = document.createElement('h4');
    defaultText.textContent = 'No items to search';
    dropBlock.append(h3, select, defaultText);
    createTableforRegion();
}
// функция для получения данных по языку

function listOfLanguages() {
    let arr2 = [];
    arr2 = externalService.getLanguagesList();
    let h3 = document.createElement('h3');
    h3.className = 'title';
    h3.innerHTML = 'Please choose search query';
    let select = document.createElement('select');
    select.id = 'mySelect';
    let option1 = document.createElement('option');
    option1.text = 'Select value';
    select.append(option1);

    for (let i = 0; i < arr2.length; i++) {
        let option = document.createElement('option');
        option.text = arr2[i];
        select.append(option);
    }
    let defaultText = document.createElement('h4');
    defaultText.textContent = 'No items to search';
    dropBlock.append(h3, select, defaultText);
}
radioButton1.addEventListener('click', listOfRegions);
radioButton2.addEventListener('click', listOfLanguages);

//фуекция для создания таблицы по регионам
function createTableforRegion() {
    let arr1 = [];
    arr1 = externalService.getRegionsList();
    let table = document.createElement('table');
    table.className = 'table';

    for (let i = 0; i < arr1.length; i++) {
        let tr = document.createElement('tr');
        console.log(arr1[i]);
        tr.className = 'table_tr';

        let td1 = document.createElement('td');
        let td2 = document.createElement('td');
        let td3 = document.createElement('td');
        let td4 = document.createElement('td');
        let td5 = document.createElement('td');
        let td6 = document.createElement('td');

        td1.innerHTML = arr1[i].name;
        td2.innerHTML = arr1[i].capital;
        td3.innerHTML = arr1[i].region;
        // td4.innerHTML = `${Object.values(arrayLang[i].language).join('')}`;
        td5.innerHTML = arr1[i].area;
        // let img = document.createElement('img');
        // img.src = arrayReg[i].flagURL;
        // td6.append(img);

        tr.appendChild(td1, td2, td3, td4, td5, td6);
        table.appendChild(tr);
        mainBlock.append(table);
    }
}
